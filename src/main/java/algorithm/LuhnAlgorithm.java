package main.java.algorithm;

public class LuhnAlgorithm {

    public boolean validateLuhn(String cardNumber) {
        if (checkUserInput(cardNumber)) {
            int sum = 0;
            // Used to only double every second digit
            boolean skip = false;
            for (int i = cardNumber.length() - 1; i >= 0; i--) {
                int n = Integer.parseInt(cardNumber.substring(i, i + 1));
                if (skip) {
                    n = multiplyByTwo(n);
                    if (n > 9) {
                        n = doubleToSingleDigit(n);
                    }
                }
                sum += n;
                skip = !skip;
            }
            // If the sum % 10 == 0, it passed the algorithm
            return (sum % 10 == 0);
        }
        return false;
    }

    public int calculateExpected(String cardNumber) {
        // Calculate the check digit for the provided number
        // Compute the sum of the digits
        // Multiply by 9
        // Mod 10
        int sum = 0;
        for (int i = 0; i < cardNumber.length(); i++) {
            sum += Integer.parseInt(String.valueOf(cardNumber.charAt(i)));
        }
        return ((sum * 9) % 10);
    }

    public int calculateProvided(String cardNumber) {
        // Returns the last digit in the provided number
        return Integer.parseInt(String.valueOf(cardNumber.charAt(cardNumber.length() - 1)));
    }

    public int doubleToSingleDigit(int number) {
        // If the number is > 9, add the digits in the number to get a single digit number.
        return (number % 10) + 1;
    }

    public int multiplyByTwo(int number) {
        // Multiply the number by 2
        return number * 2;
    }

    public boolean checkUserInput(String cardNumber) {
        // Check if the input is a number and if the length is 16
        if (cardNumber.matches("[0-9]+") && cardNumber.length() == 16) {
            System.out.println("Input: " + cardNumber + " " + cardNumber.charAt(cardNumber.length() - 1));
            System.out.println("Provided: " + calculateProvided(cardNumber));
            System.out.println("Expected: " + calculateExpected(cardNumber));
            return true;
        } else {
            System.out.println("Input: " + cardNumber + " is not a valid number");
            System.out.println("Provided: Invalid");
            System.out.println("Expected: Invalid");
            return false;
        }
    }
}
