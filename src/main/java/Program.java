package main.java;

import main.java.algorithm.LuhnAlgorithm;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        LuhnAlgorithm luhnAlgorithm = new LuhnAlgorithm();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Luhn Algorithm");
        System.out.println("Validate your credit card number\n");
        System.out.print("Enter a 16 digit credit card number:");
        String cardNumber = scanner.next();

        if(luhnAlgorithm.validateLuhn(cardNumber)){
            System.out.println("Checksum: Valid");
        }
        else{
            System.out.println("Checksum: Invalid");
        }
        System.out.println("Digits: " + cardNumber.length());

    }
}
