package main.java.algorithm;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnAlgorithmTest {
    LuhnAlgorithm luhnAlgorithm = new LuhnAlgorithm();

    @Test
    void testIsDoubleDigit() {
        assertEquals(3, luhnAlgorithm.doubleToSingleDigit(12));
        assertEquals(9, luhnAlgorithm.doubleToSingleDigit(18));
        assertEquals(5, luhnAlgorithm.doubleToSingleDigit(14));
    }

    @Test
    void testMultiplyByTwo() {
        assertEquals(4, luhnAlgorithm.multiplyByTwo(2));
        assertEquals(8, luhnAlgorithm.multiplyByTwo(4));
        assertEquals(14, luhnAlgorithm.multiplyByTwo(7));
    }

    @Test
    void testCalculateProvided() {
        assertEquals(2, luhnAlgorithm.calculateProvided("4242424242424242"));
        assertEquals(1, luhnAlgorithm.calculateProvided("4111111111111111"));
        assertEquals(4, luhnAlgorithm.calculateProvided("5500000000000004"));
    }

    @Test
    void testCalculateExpected(){
        assertEquals(2, luhnAlgorithm.calculateExpected("4242424242424242"));
        assertEquals(1, luhnAlgorithm.calculateExpected("4111111111111111"));
        assertEquals(6, luhnAlgorithm.calculateExpected("5500000000000004"));
    }

    @Test
    void testValidateLuhn(){
        assertTrue(luhnAlgorithm.validateLuhn("4242424242424242"));
        assertTrue(luhnAlgorithm.validateLuhn("5500000000000004"));
        assertTrue(luhnAlgorithm.validateLuhn("4111111111111111"));
        assertFalse(luhnAlgorithm.validateLuhn("23452434"));
        assertFalse(luhnAlgorithm.validateLuhn("41111112345323111111"));
        assertFalse(luhnAlgorithm.validateLuhn("0"));
    }

    @Test
    void testUserInput(){
        assertTrue(luhnAlgorithm.checkUserInput("4242424242424242"));
        assertTrue(luhnAlgorithm.checkUserInput("5500000000000004"));
        assertFalse(luhnAlgorithm.checkUserInput("23452434"));
        assertFalse(luhnAlgorithm.checkUserInput("thisisnotanumber"));
    }
}