# Luhn-Algorithm

This is a console application that allows you to test a credit card number, to see if it passes the Luhn Algorithm.
The number must be 16 digits long.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

You can start by downloading the project in a .zip file, or cloning the project using the terminal.

### Prerequisites

What things you need to install the software and how to install them

Install the latest version of Java JDK: https://www.oracle.com/java/technologies/javase-jdk14-downloads.html


### Running the project

Open the project in an IDE of your choice and run the Program.java class.

### Testing

Under the "test" folder, you will find a LuhnAlgorithmTest file, that contains several tests using JUnit 5.

## Authors

* **Robin Burø** - *Initial work* - [Robbur](https://gitlab.com/Robbur)

